# TorrentYks

Skynet over BitTorrent - Upload a file to skynet and give to your friend a torrent


Have you ever shared files through the bittorrent protocols - A well-know uncentralized file sharing?<br>
Have you ever experienced dead torrent?<br>
**No more** with TorrentYks!<br>
Upload a file, create a torrent and share it without even seeding it:
 - Higher speeds
 - Lower latency
 - Higher availability
 - More secure
 - Truly uncensored
Helped by real peers, improved by skynet network! **Enjoy!**

## User part

You can see here the last version of the web page in [the release area](https://gitlab.com/Kcchouette/torrentyks/-/releases).

## Developpment part

### Installation

Install npm and the dependancies we have with it:
```
$ npm install browserify
$ npm install skynet-js
$ npm install file-saver
$ npm install create-torrent
$ npm install drag-drop
$ npm install upload-element
$ npm install escape-html
```

### Building

```
$ browserify main.js -o dist/bundle.js
```

## Ideas

 - Adding webtorrent seeding
 - Adding progress bar

## FAQ

### How long are files available for?

We build this apps to keep files indefinitely on skynet/bitorrent.

## Disclaimer

SkyPerf-Portal is an experiment and under active development. The answers here may change as we get feedback from you and the project matures.