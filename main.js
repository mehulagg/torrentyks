const dragDrop = require('drag-drop')
const createTorrent = require('create-torrent')
const FileSaver = require('file-saver')
const skynet = require('skynet-js');
const uploadElement = require('upload-element')
const escapeHtml = require('escape-html')

const client = new skynet.SkynetClient("https://siasky.net");

// When user drops files on the browser, create a new torrent and start seeding it!
dragDrop('drag', onUploadFiles)

// Seed via upload input element
var upload = document.querySelector('input[name=upload]')
if (upload) {
  uploadElement(upload, function (err, files) {
    if (err) return util.error(err)
    files = files.map(function (file) { return file.file })
    onUploadFiles(files)
  })
  }

function onUploadFiles(files, pos, fileList, directories) {
  console.log("files", files)
  var url = []
  var nameOfTorrent
  fetch('https://kcchouette.github.io/public-skynet-portal/portals.json')
    .then(function (response) {
      return response.json();
    })
    .then(function (json) {
      
      let uploadFile

      if (files.length === 1) {
        uploadFile = client.upload(files[0])
        nameOfTorrent = files[0].name
      }
      else {

        nameOfTorrent = fileList[0].name

        const directory = files.reduce((accumulator, file) => {
          const path = skynet.getRelativeFilePath(file);
          return { ...accumulator, [fileList[0].name + "/" + path]: file };
        }, {});

        uploadFile = client.uploadDirectory(directory, fileList[0].name)
      }

      
      //client.upload(files[0])
      uploadFile.then(function(skylink) {
        console.debug(skylink);

        json.forEach(function (item) {
          console.debug(item["portal_url"]);
          let endOfFile = ""
          if (files.length === 1) {
            endOfFile = files[0].name
          }

          url.push(item["portal_url"] + "/" + skylink["skylink"] + "/" + endOfFile);

        });

        createTorrent(files, { urlList: url }, (err, torrent) => {
          if (!err) {
            // `torrent` is a Buffer with the contents of the new .torrent file
            console.debug('Client is building:', torrent)

            upload.value = upload.defaultValue // reset upload element

            // Note: Ie and Edge don't support the new File constructor,
            // so it's better to construct blobs and use saveAs(blob, filename)
            var file = new File([torrent], escapeHtml(nameOfTorrent)+".torrent", { type: "application/x-bittorrent" });
            FileSaver.saveAs(file);
          }
        })

      })

    });
}